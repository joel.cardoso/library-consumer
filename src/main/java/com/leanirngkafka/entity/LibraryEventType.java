package com.leanirngkafka.entity;

public enum LibraryEventType {
    NEW,
    UPDATE
}
